jQuery( document ).ready( function( $ ) {
    $('.tstbttns-city-select').select2({
        minimumInputLength: 3,
        closeOnSelect: true,
        placeholder :'Choose the city',
        ajax: {
            url: tstbttns_js.ajax_url,
            data: function(params){
                var query = {
                    action: 'tstbttns_search_city',
                    search: params.term
                }
                return query;
            }
        }
    });
    $('.tstbttns-calculate-delivery').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        form.find('.tstbttns-result').text('');
        var data = {
            action : 'tstbttns_np_calculate_delivery',
            city_from : form.find('#tstbttns-city-from').val(),
            city_to : form.find('#tstbttns-city-to').val(),
            price: form.find('#tstbttns-total-price').val(),
            weight: form.find('#tstbttns-total-weight').val()
        }
        $.post(
            tstbttns_js.ajax_url,
            data,
            function(response){
                if(response.success) {
                    form.find('.tstbttns-result').text(tstbttns_js.delivery_price_text + response.data['cost'])
                } else {
                    form.find('.tstbttns-result').text(response.data['message'])
                }
            }
        );
    })
} );