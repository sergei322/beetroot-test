<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'beetroot' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~x,*Un&|6SVS-oi*?!J{c;{SWW.^^&$ IfnN;1gwd;1YqEMgI0Hgq7z)tQW89Z?n' );
define( 'SECURE_AUTH_KEY',  'ra.QepNai8>w,a9de-%&coyEJqF=a,mL,mNc[}E Q2Pd*A(0}ts(@*sF}dX8:+M2' );
define( 'LOGGED_IN_KEY',    '4Zwds))O~ty]@TKSt1ZR)&jk!GuJG[yNcptEbAw_@==Xj`Sej)P(rD}*PTA~.,1N' );
define( 'NONCE_KEY',        '`ig/c)5^/{pXpw~0&kApNl_K_TgFJW:CAuH3i`^u`5R807||g)*3K`,@GnmiU=B]' );
define( 'AUTH_SALT',        '-67FB/75gS.kVil4?O}7ca%%ggP!yuim9j<(dp7%2t?s?kx4<QyRv`LB@J>]]vcl' );
define( 'SECURE_AUTH_SALT', 'N&lZkQjvUR![?&gT<d#ed twx>Iv9A>,.ed.HNg$yIBJ*k8~(2hcf>wNtSPaR+^j' );
define( 'LOGGED_IN_SALT',   'Fn(5{!!fFkb9uVok]a,KveA1]2jyMn$?_58G!R+Ef5K.@fez2AIQMC+/{:M[H3jk' );
define( 'NONCE_SALT',       'R*rZO2%1x>KAHQ{Z _nMp%]_54Mh=:Fe1Nh@bDxpG8~2be/{r7[A5}yz{@CjL=Dq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

define( 'FS_METHOD', 'direct' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

