<?php 
/*
Plugin Name: Test Buttons
Version: 1.0.0
Description: Shortcode Buttons to show Next Games and Calculate Delivery
Text Domain: tstbttns
*/


// Next Games Shortcode
if( !function_exists( 'tstbttns_next_games' ) ) {
    function tstbttns_next_games() {
        if( is_singular( 'event' ) ) {
            $button  =  '<div class="tstbttns-next-games">';
            $button .=      '<button>';
            $button .=          __( 'Next Games', 'tstbttns' );
            $button .=      '</button>';
            $button .=  '</div>';
            return $button;
        } 
        return;
    }
    add_shortcode( 'tstbttns_next_games', 'tstbttns_next_games' );
}

// Next Games Shortcode styles && scripts
if( !function_exists( 'tstbttns_enqueue_next_games_script' ) ) {
    function tstbttns_enqueue_next_games_script() {
        if( is_singular( 'event' ) ) {
            global $post;
            if( has_shortcode( $post->post_content, 'tstbttns_next_games' ) ) {
                wp_enqueue_script( 'tstbttns-next-game-button', plugins_url( '/assets/js/next-game-button.js', __FILE__ ), array( 'jquery' ) );
                $ajax_array = array(
                    'ajax_url'  => admin_url( 'admin-ajax.php' ),
                    'event_id'  => $post->ID
                );
                wp_localize_script( 'tstbttns-next-game-button', 'tstbttns_js', $ajax_array );
            }
        }
    }
    add_action( 'wp_enqueue_scripts', 'tstbttns_enqueue_next_games_script' );
}

// Next Games Shortcode Ajax
if( !function_exists( 'tstbttns_load_next_games' ) ) {
    function tstbttns_load_next_games() {
        global $wpdb;
        $time_now = date( 'Y-m-d H:i:s' );
        if( !empty( $_POST['event_id'] ) ) {
            $event_start = get_post_meta( $_POST['event_id'], '_event_start_local', true );
            if( $event_start < $time_now ) {
                $event_start = $time_now;
            }
        } else {
            $event_start = $time_now;
        }

        $next_events = $wpdb->get_results( $wpdb->prepare(
            "SELECT p.ID, p.post_title, pm1.meta_value as 'event_start'
            FROM " . $wpdb->posts . " p
            LEFT JOIN " . $wpdb->postmeta . " pm1
                ON p.ID = pm1.post_id
            WHERE post_status = 'publish'
                AND p.post_type = 'event'
                AND pm1.meta_key = '_event_start_local'
                AND pm1.meta_value > %s"
            , $event_start
        ), ARRAY_A );
        $data = array();
        if( !empty( $next_events ) ) {
            foreach( $next_events as $id => $next_event ) {
                $next_events[$id]['permalink'] = get_the_permalink( $next_event['ID'] );
                if( has_post_thumbnail( $next_event['ID'] ) ) {
                    $next_events[$id]['thumb'] = get_the_post_thumbnail( $next_event['ID'], 'thumbnail' );
                } else {
                    $next_events[$id]['thumb'] = '';
                }
            }
            $data['events'] = $next_events;
            wp_send_json_success( $data );
        } else {
            $data['message'] = __( 'There are no next events', 'tstbttns' );
            wp_send_json_error( $data );
        }
    }
    add_action( 'wp_ajax_load_next_games', 'tstbttns_load_next_games' );
    add_action( 'wp_ajax_nopriv_load_next_games', 'tstbttns_load_next_games' );
}

// Add Calculate Delivery Settings Page
if( !function_exists( 'tstbttns_calculate_delivery_settings_page' ) ) {
    function tstbttns_calculate_delivery_settings_page() {
        add_menu_page( 
            __( 'Calculate Delivery Settings', 'tstbttns' ),
            __( 'Calculate Delivery Settings', 'tstbttns' ),
            'manage_options',
            'tstbttns_delivery_settings',
            'tstbttns_delivery_settings_page'
        );
    }
    add_action( 'admin_menu', 'tstbttns_calculate_delivery_settings_page' );
}

// Register Delivery Options
if( !function_exists( 'tstbttns_delivery_register_settings' ) ) {
    function tstbttns_delivery_register_settings() {
        register_setting( 'tstbttns_delivery', 'tstbttns_np_api_key' );
    }
    add_action( 'admin_init', 'tstbttns_delivery_register_settings' );
}

// Delivery SettingsPage
if( !function_exists( 'tstbttns_delivery_settings_page' ) ) {
    function tstbttns_delivery_settings_page() { ?>
        <div class="wrap">
            <h1><?php _e( 'Calculate Delivery Settings', 'tstbttns' ); ?></h1>
            <form method="post" action="options.php">
                <?php settings_fields( 'tstbttns_delivery' ); ?>
                <?php do_settings_sections( 'tstbttns_delivery' ); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">
                            <?php _e( 'Nova Poshta API Key', 'tstbttns' ) ?>
                        </th>
                        <td>
                            <input type="text" name="tstbttns_np_api_key" value="<?php echo esc_attr( get_option( 'tstbttns_np_api_key' ) ); ?>" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    <?php }
}


// Calculate Delivery Shortcode
if( !function_exists( 'tstbttns_calculate_delivery' ) ) {
    function tstbttns_calculate_delivery() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
        if( in_array( 'woocommerce/woocommerce.php', $active_plugins ) ) {
            if( is_checkout() ) {
                global $woocommerce;
                $amount = floatval( $woocommerce->cart->total );
                $weight = floatval( $woocommerce->cart->cart_contents_weight );
                $delivery_form  =  '<form class="tstbttns-calculate-delivery">';
                $delivery_form .=      '<p class="form-row form-row-first">';
                $delivery_form .=          '<label for="tstbttns-total-price">' . __( 'Total Price:', 'tstbttns' ) . '</label>';
                $delivery_form .=          '<span class="woocommerce-input-wrapper">';
                $delivery_form .=              '<input class="input-text" type="text" id="tstbttns-total-price" value="' . $amount . '">';
                $delivery_form .=          '</span>';
                $delivery_form .=      '</p>';
                $delivery_form .=      '<p class="form-row form-row-last">';
                $delivery_form .=          '<label for="tstbttns-total-weight">' . __( 'Weight:', 'tstbttns' ) . '</label>';
                $delivery_form .=          '<span class="woocommerce-input-wrapper">';
                $delivery_form .=              '<input class="input-text" type="text" id="tstbttns-total-weight" value="' . $weight . '">';
                $delivery_form .=          '</span>';
                $delivery_form .=      '</p>';
                $delivery_form .=      '<p class="form-row form-row-first">';
                $delivery_form .=          '<label for="tstbttns-city-from">' . __( 'City From:', 'tstbttns' ) . '</label>';
                $delivery_form .=          '<span class="woocommerce-input-wrapper">';
                $delivery_form .=              '<select id="tstbttns-city-from" class="tstbttns-city-select" name="tstbttns-city-from">';
                $delivery_form .=              '</select>';
                $delivery_form .=          '</span>';
                $delivery_form .=      '</p>';
                $delivery_form .=      '<p class="form-row form-row-last">';
                $delivery_form .=          '<label for="tstbttns-city-to">' . __( 'City To:', 'tstbttns' ) . '</label>';
                $delivery_form .=          '<span class="woocommerce-input-wrapper">';
                $delivery_form .=              '<select id="tstbttns-city-to" class="tstbttns-city-select" name="tstbttns-city-to">';
                $delivery_form .=              '</select>';
                $delivery_form .=          '</span>';
                $delivery_form .=      '</p>';
                $delivery_form .=      '<button type="submit">' . __( 'Calculate delivery', 'tstbttns' ) .'</button>';
                $delivery_form .=      '<div class="tstbttns-result"></div>';
                $delivery_form .=  '</form>';
                return $delivery_form;
            }
        }
        return;
    }
    add_shortcode( 'tstbttns_calculate_delivery', 'tstbttns_calculate_delivery' );
}

// Calculate Delivery Shortcode styles && scripts
if( !function_exists( 'tstbttns_enqueue_calculate_delivery_script' ) ) {
    function tstbttns_enqueue_calculate_delivery_script() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
        if( in_array( 'woocommerce/woocommerce.php', $active_plugins ) ) {
            if( is_checkout() ) {
                global $post;
                if( has_shortcode( $post->post_content, 'tstbttns_calculate_delivery' ) ) {
                    // No need to enqueue select2 scripts - use woocommerce select2
                    wp_enqueue_script( 'tstbttns-calculate-delivery-button', plugins_url( '/assets/js/calculate-delivery-button.js', __FILE__ ), array( 'jquery', 'select2' ) );
                    $ajax_array = array(
                        'ajax_url'  => admin_url( 'admin-ajax.php' ),
                        'delivery_price_text' => __( 'Delivery price:', 'tstbttns' ),
                    );
                    wp_localize_script( 'tstbttns-calculate-delivery-button', 'tstbttns_js', $ajax_array );
                }
            }
        }
    }
    add_action( 'wp_enqueue_scripts', 'tstbttns_enqueue_calculate_delivery_script' );
}

// Calculate Delivery city search Ajax
if( !function_exists( 'tstbttns_search_city' ) ) {
    function tstbttns_search_city() {
        $np_api_key = esc_attr( get_option( 'tstbttns_np_api_key' ) );
        require_once('includes/nova-poshta/NovaPoshtaApi2.php');
        $np = new LisDev\Delivery\NovaPoshtaApi2(
            $np_api_key,
            'ua',
            FALSE,
            'curl'
        );
        $search = !empty( $_REQUEST['search'] ) ? $_REQUEST['search'] : '';
        $result = $np
            ->model( 'Address' )
            ->method( 'searchSettlements' )
            ->params( array(
                'CityName' => $search,
                'Limit' => -1
            ) )
            ->execute();
        $results = array();
        $pagination = false;

        if( $result['success'] && !empty( $result['data'][0]['Addresses'] ) ) {
            foreach( $result['data'][0]['Addresses'] as $city ) {
                $results[] = array(
                    'id' => $city['Ref'],
                    'text' => $city['Present']
                );
            }
        }
        $return = array(
            'results' => $results,
            'pagination' => $pagination
        );
        wp_send_json( $return );
    }
    add_action( 'wp_ajax_tstbttns_search_city', 'tstbttns_search_city' );
    add_action( 'wp_ajax_nopriv_tstbttns_search_city', 'tstbttns_search_city' );
}

// Calculate Delivery calculate Ajax
if( !function_exists( 'tstbttns_np_calculate_delivery' ) ) {
    function tstbttns_np_calculate_delivery() {
        $city_from = !empty( $_POST['city_from'] ) ? $_POST['city_from'] : '';
        $city_to = !empty( $_POST['city_to'] ) ? $_POST['city_to'] : '';
        $price = ( !empty( $_POST['price'] ) && is_numeric( $_POST['price'] ) ) ? floatval( $_POST['price'] ) : '1';
        $weight = ( !empty( $_POST['weight'] ) && is_numeric( $_POST['weight'] ) ) ? floatval( $_POST['weight'] ) : '1';
        $data = array();
        if( !empty( $city_from ) && !empty( $city_to ) ) {
            $np_api_key = esc_attr( get_option( 'tstbttns_np_api_key' ) );
            require_once('includes/nova-poshta/NovaPoshtaApi2.php');
            $np = new LisDev\Delivery\NovaPoshtaApi2(
                $np_api_key,
                'ua',
                FALSE,
                'curl'
            );
            $result = $np->getDocumentPrice($city_from, $city_to, 'WarehouseWarehouse', $weight, $price);
            if( $result['success'] ) {
                $data['cost'] = $result['data'][0]['Cost'];
                wp_send_json_success( $data );
            } else {
                $data['message'] = implode( $result['errors'] );
                wp_send_json_error( $data );
            }
        } else {
            $data['message'] = __( 'Please fill the cities fields', 'tstbttns' );
            wp_send_json_error( $data );
        }
    }
    add_action( 'wp_ajax_tstbttns_np_calculate_delivery', 'tstbttns_np_calculate_delivery' );
    add_action( 'wp_ajax_nopriv_tstbttns_np_calculate_delivery', 'tstbttns_np_calculate_delivery' );
}

// Delete Options on Uninstall


if( !function_exists( 'tstbttns_uninstall' ) ) {
    function tstbttns_uninstall() {
        delete_option( 'tstbttns_np_api_key' );
    }
    register_uninstall_hook( __FILE__, 'tstbttns_uninstall' );
}
