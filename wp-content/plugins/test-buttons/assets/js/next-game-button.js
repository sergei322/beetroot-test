jQuery( document ).ready( function( $ ) {
    $('.tstbttns-next-games button').on('click', function(e){
        e.preventDefault();
        wrapper = $(this).closest('.tstbttns-next-games');
        var data = {
            action: 'load_next_games',
            event_id: tstbttns_js.event_id
        }
        $.post(
            tstbttns_js.ajax_url,
            data,
            function(response){
                if(response.success) {
                    table = renderEventsTable(response.data['events']);
                    wrapper.html(table);
                } else {
                    wrapper.html(response.data['message']);
                }
            }
        )
    })
} );

function renderEventsTable(events) {
    table  = '<table>';
    table +=    '<thead>';
    table +=        '<tr>';
    table +=            '<th>Date/Time</th>';
    table +=            '<th>Event</th>';
    table +=        '</tr>';
    table +=    '</thead>';
    table +=    '<tbody>';
    jQuery.each(events, function(index, value){
        table +=    '<tr>';
        table +=        '<td>'+value.event_start+'</td>';
        table +=        '<td>';
        table +=            value.thumb;
        table +=            '<a href="'+value.permalink+'">'+value.post_title+'</a>';
        table +=        '</td>';
        table +=    '</tr>';
    });
    table +=    '</tbody>';
    table += '</table>';
    return table;
}