<?php 
/* Enqueue main parent style */
if( !function_exists('tk_child_enqueue_styles') ) {
    function tk_child_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    }
    add_action( 'wp_enqueue_scripts', 'tk_child_enqueue_styles' );
}

/* Show related products for the events */
if( !function_exists( 'tkchld_events_related_products' ) ) {
    function tkchld_events_related_products( $content ) {
        global $post;
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
        // check if WooCommerce and Events Manager are active
        if( in_array( 'woocommerce/woocommerce.php', $active_plugins ) 
        && in_array( 'events-manager/events-manager.php', $active_plugins )
        // show only on the single event pages
        && is_singular( 'event' )
        // check if the booking form is not prevented to show
        && ( !defined( 'EM_DISABLE_AUTO_BOOKINGSFORM') || !EM_DISABLE_AUTO_BOOKINGSFORM ) ) {
            // get featured products
            $registration_enabled = get_post_meta( $post->ID, '_event_rsvp', true );
            $featured_products = get_post_meta( $post->ID, 'featured_products', true );
            if( !empty( $featured_products ) && !empty( $registration_enabled ) ) {
                ob_start(); ?>
                <section class="events-featured-products products">
                    <h2><?php esc_html_e( 'Featured products' ); ?></h2>
                    <?php woocommerce_product_loop_start();
                    foreach( $featured_products as $featured_product ) {
                        $product_object = get_post( $featured_product );
                        setup_postdata( $GLOBALS['post'] =& $product_object );
                        wc_get_template_part( 'content', 'product' );
                    }
                    woocommerce_product_loop_end(); ?>
                </section>
                <?php wp_reset_postdata();
                $featured_products_content = ob_get_clean();
                $content .= $featured_products_content;
            }
        }
        return $content;
    }
    add_action( 'the_content', 'tkchld_events_related_products' );
}

// Add woocommerce class to the body if showing event featured products
if( !function_exists( 'tkchld_add_woo_body_class' ) ) {
    function tkchld_add_woo_body_class( $classes ) {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
        // check if WooCommerce and Events Manager are active
        if( in_array( 'woocommerce/woocommerce.php', $active_plugins ) 
        && in_array( 'events-manager/events-manager.php', $active_plugins )
        // show only on the single event pages
        && is_singular( 'event' )
        // check if the booking form is not prevented to show
        && ( !defined( 'EM_DISABLE_AUTO_BOOKINGSFORM') || !EM_DISABLE_AUTO_BOOKINGSFORM ) ) {
            $classes[] = 'woocommerce';
        }
        return array_unique( $classes );
    }
    add_filter( 'body_class', 'tkchld_add_woo_body_class' );
}